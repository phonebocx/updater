<?php

include __DIR__.'/vendor/autoload.php';

error_reporting(E_ALL);

if (!file_exists(__DIR__.'/config.json'))
{
	print "No config.json\n";
	exit;
}

$conf = @json_decode(file_get_contents(__DIR__.'/config.json'), true);

$header = [
	"User-Agent" => "github.com/phonebocx",
	"Accept" => "application/json",
	"Authorization" => "token ".$conf['token']
];

$req = Requests::get('https://api.github.com/orgs/phonebocx/repos', $header);

if ($req->status_code !== 200)
{
	print "Error\n";
	exit;
}

if (!isset($req->headers['link']))
{
	print "No link\n";
	exit;
}

$repos = [];

while (true)
{
	addToRepos($header, $repos, $req);
	$links = $req->headers['link'];
	$url = getNextLink($links);
	if ($url === false)
	{
		break;
	}
	$req = Requests::get($url, $header);
}

foreach ($repos as $r)
{
	$module = $r['name'];
	$src = $r['ssh_url'];
	$dst = __DIR__."/repos/$module";
	if (!is_dir($dst))
	{
		$check = @mkdir($dst);
		if (!$check)
		{
			print "Can't mkdir $dst\n";
			exit;
		}
	}
	if (!is_dir("$dst/.git"))
	{
		cloneRepo($src, $dst);
	}
	if (isset($r['parent']))
	{
		updateRepo($dst, $r['parent']);
	}
}

function cloneRepo($url, $dst)
{
	print "Cloning $url into $dst\n";
	`git clone $url $dst`;
}

function updateRepo($dir, $repo)
{
	$git = new PHPGit\Git();
	$git->setRepository($dir);
	$remote = $git->remote();
	if (!isset($remote['upstream']))
	{
		$git->remote->add("upstream", $repo['ssh_url']);
	} else {
		if ($remote['upstream']['fetch'] !== $repo['ssh_url'])
		{
			$git->remote->rm("upstream");
			$git->remote->add("upstream", $repo['ssh_url']);
		}
	}
	chdir($dir);
	$git->fetch->all(['prune' => 'true']);
	$branches = $git->branch();
	$remotebranches = $git->branch(['remotes' => true]);
	foreach ($remotebranches as $branch => $null)
	{
		if (strpos($branch, "origin") === 0) {
			continue;
		}
		$name = str_replace("upstream/", "", $branch);
		if (!isset($branches[$name]))
		{
			`git checkout -b $name $branch`;
		} else {
			`git checkout $name`;
		}
		`git rebase $branch`;
		`git push origin $name`;
	}
}

function addToRepos($header, &$repos, $req)
{
	$j = @json_decode($req->body, true);
	if (!is_array($j))
	{
		throw new Exception("Not json");
	}
	foreach ($j as $row)
	{
		$url = 'https://api.github.com/repos/phonebocx/'.$row['name'];
		print "Polling $url\n";
		$repo = Requests::get($url, $header);
		if ($repo->status_code !== 200)
		{
			print "Error asking for $url\n";
			exit;
		}
		$repos[$row['name']] = json_decode($repo->body, true);
	}
}

function getNextLink($links)
{
	$sections = explode(",", $links);
	foreach ($sections as $i)
	{
		if (preg_match('!<(https://api.+)>; rel="next"!', $i, $out))
		{
			return($out[1]);
		}
	}
	return false;
}


